import time
from ggplot import *
import pandas as pd
import argparse
import functools

#@functools.lru_cache(maxsize=None) # memoization slower than selfmade line 15!
def fib_recursive(num):
    if num < 2:
        return num
    else:
        return fib_recursive(num - 1) + fib_recursive(num - 2)


def fib_recursive_memoized(n):
    cache = {0: 0, 1: 1}

    def fib(n):
        if n in cache:
            return cache[n]
        cache[n] = fib(n - 1) + fib(n - 2)
        return cache[n]

    return fib(n)


def recursive(n, memoized=False):
    timestamps = []
    start = time.process_time()
    row = []
    for i in range(1, n):
        if memoized:
            row.append(fib_recursive_memoized(i))
        else:
            row.append(fib_recursive(i))
        timestamps.append(time.process_time() - start)

    return row, timestamps


def iterative(n):
    timestamps = []
    start = time.process_time()
    row = [1]
    a, b = 1, 1
    for i in range(n - 1):
        a, b = b, a + b
        row.append(a)
        timestamps.append(time.process_time() - start)

    return row, timestamps


parser = argparse.ArgumentParser(usage="fibbonaci.py [-h] [-r] [-m] length\n"
                                       "Returns the Fibunacci sequence of a certain lenght and "
                                       " plots the processing times\n")
parser.add_argument("length", help="integer length of fibonacci sequence", type=int)
parser.add_argument("-r", action="store_true", help="do not use recursive function")
parser.add_argument("-m", action="store_true", help="do not use recusion with memoization")
args = parser.parse_args()
parser.print_usage()
n = args.length
maximum = 30
row, ite = iterative(n + 1)
df = pd.DataFrame()
x = [i for i in range(1, n + 1)]
df['sequence length'] = x
df["iterate"] = ite
if not args.r:
    if n > maximum:
        n = maximum
        print("Sequence length set to maximum of {}".format(str(maximum)))
    row3, rec = recursive(n + 1)
    df["recurse"] = rec

if not args.m:
    row4, recm = recursive(n + 1, memoized=True)
    df["recurse\nmemoized"] = recm

df = pd.melt(df, id_vars='sequence length')
g = ggplot(aes(x='sequence length', y='value', color='variable'), df) + geom_line() + \
    ggtitle("Fibonacci") + labs(y="time [sec]")
if n < 30:
    g = g + scale_x_continuous(breaks=x)

print("The Fibonacci sequence from 1 to length {}:\n{}".format(n, ", ".join([str(i) for i in row])))
print(g)
